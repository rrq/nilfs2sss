#!/bin/bash
#
# Snapshot Snapping Scheme
#
# This is a cron bot to manage the snapshots of an nilfs2 file system,
# so as to preserve 7 most recent daily, 4 most recent weekly and 12
# most recent monthly snapshots, in addition to labelled ones.
#
# The cron bot is expected to run once a minute

# Name of snapshot tag file for "important" snapshots. Format:
# $CNO .... one line for each $CNO that should be preserved as snapshot
# chcp ss $CNO && echo $CNO >> $NAMELIST
NAMELIST=/etc/nilfs2sss.list

# The hour-of-day to preserve as daily snapshot
KEEPHH=12

# The day-of-month to preserve as monthly snapshot
KEEPDD=15

DEV=$1
if [ -z "$DEV" ] ; then
    DEVS=( $( mount | grep nilfs2 | sed 's/ .*//' ) )
    DEV=${DEVS[0]}
    if [ -z "$DEV" ] ; then
	echo "Error* cannot find a mounted nilfs2 partition" >&2
	exit 1
    fi
fi

FIVELY="$(date -d '-5 minutes' '+%Y-%m-%d %H:%M:%S')"
HOURLY="$(date -d '-1 hour' '+%Y-%m-%d %H:%M:%S')"
DAILY="$(date -d '-1 day' '+%Y-%m-%d %H:%M:%S')"
MONTHLY="$(date -d '-1 month' '+%Y-%m-%d %H:%M:%S')"
WEEKLY="$(date -d '-7 days' '+%Y-%m-%d %H:%M:%S')"
YEARLY="$(date -d '-1 year' '+%Y-%m-%d %H:%M:%S')"

MARK=( )
function marksnap() {
    if [[ "${MARK[0]}" == "$1" ]] && [[ "${MARK[1]}" = "$2" ]] ; then
	if [ -r "$NAMELIST" ] && grep -q ^$3 "$NAMELIST" ; then
	    : # Don't change important snapshots
	else
	    chcp cp $DEV ${MARK[2]}
	fi
    fi
    MARK=( "$1" "$2" "$3" )
}

{ flock 9

  date "+$DEV: %Y-%m-%d %H:%M:%S ---- checking" >&2
  lscp -s $DEV | while read CNO DATE TIME REST ; do
      TS="$DATE $TIME"
      [ "$CNO" = "CNO" ] && continue
      if [[ ! "$TS" > "$YEARLY" ]] ; then
	  marksnap YEAR "${DATE:0:4}" $CNO
      elif [[ ! "$TS" > "$MONTHLY" ]] ; then
	   marksnap MONTH "${DATE:5:2}" $CNO
      elif [[ ! "$TS" > "$WEEKLY" ]] ; then
	   # For older than most recent week, keep last snapshot each week
	   marksnap WEEK "$(( 7${DATE:8:2} / 7 - 100 ))" $CNO
      elif [[ ! "$TS" > "$DAILY" ]] ; then
	   marksnap DAY "${DATE:8:2}" $CNO
      elif [[ ! "$TS" > "$HOURLY" ]] ; then
	   marksnap HOUR "${DATE:8:2}-${TIME:0:2}" $CNO
      else
	  : # echo "$CNO $DATE $TIME within last hour"
      fi
  done | if read X ; then
      : # snapshot within last hour
  else
      date "+$DEV: new snapshot at %Y-%m-%d %H:%M:%S" >&2
      mkcp -s $DEV
  fi
} 9> /var/run/lock/sss.lock
