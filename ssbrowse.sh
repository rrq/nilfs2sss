#!/bin/bash
#
# Browse snapshots of a given path and run meld to compare with
# current directory
# $1: path -- for mounted nilfs

## Prepare for mounting of snapshots
MOUNT=/tmp/ssmeld-$$
mkdir -p $MOUNT || exit 1

function exitbrowse() {
    umount $MOUNT 2>/dev/null
    rmdir $MOUNT
    trap "" 0
}
trap "exitbrowse" 0 2 9 15

ARG1="${1-/backup}"
DF=( $(df $ARG1 | awk '{d=$1;p=$NF;} END {printf "%s %s", d, p;}') )

NILFSROOT="${DF[1]}"
NILFSDEV="${DF[0]}"
PTR="${ARG1#$NILFSROOT}"
DIR0="$MOUNT$PTR"
DIR1="$NILFSROOT"

function snap2iselect() {
    lscp -r -s $NILFSDEV  | while read CNO DATE TIME REST ; do
	[ $CNO = CNO ] && continue
	echo "  <s:$CNO> [$CNO] ${DATE} ${TIME}"
    done
}

function dirof() {
    x="${1%%|*}"
    x="${x%%(*}"
    echo "${x}" | sed 's/\(\S\+\s\+\)\{8\}//;s/\s*$//'
}

function dirsdiff() {
    while true ; do
	LEFT="$(cd $DIR0/${1#/} && ls -l)"
	RIGHT="$(cd $DIR1/${1#/} && ls -l)"
	SEL="$(diff -W 500 -y --left-column <(echo "$LEFT") <(echo "$RIGHT")| \
	     column -t | \
	     iselect -n "($CNO ~ $DIR1)" -K -kd -kx -kz -a -t "${1-/}" )"
	KEY="${SEL%%:*}"
	SEL="${SEL#*:}"
	[ -z "$SEL" ] && break
	case "$KEY" in
	    RETURN|KEY_RIGHT)
		ZEL="$(echo "${SEL}" | sed 's/^>\s*//;s/\s*$//')"
		if [ -z "${ZEL##d*}" ] ; then
		    D="$(dirof "$ZEL")"
		    dirsdiff "$1/$D" || return 1
		else
		    D="$(dirof "$ZEL")"
		    diff -D "$CNO" -N $DIR0/${1#/}/$D $DIR1/${1#/}/$D |\
			less
			 
		fi
		;;
	    d) # Find next dept-first difference 
		:
		;;
	    x) # Exit
		return 1
		;;
	    *)
		echo "*** ${KEY}:${SEL}"
		read x
		;;
	esac
    done
}

N=2
while true ; do
    SNAPS="$(snap2iselect)"
    CNO="$(iselect -n "($DIR1/)" \
		  -t "Snapshots for $NILFSDEV=$NILFSROOT" -p $N -P \
		  "Select snapshot" "$SNAPS")"
    [ -z "$CNO" ] && break
    N=${CNO%%:*}
    CNO=${CNO#*:}
    if sudo mount -t nilfs2 -oro,cp=$CNO $NILFSDEV $MOUNT ; then
	dirsdiff /
	sudo umount $MOUNT
    fi
done
